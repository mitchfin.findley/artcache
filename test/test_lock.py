# Import Standard Lib Modules
import shutil
import unittest
import argparse
import sys
import os
import time
import random
import tempfile
import copy

# Try to import typing for support
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

sys.dont_write_bytecode = True

# Import Local Lib Modules
import artcache.utils.cliargs as cliargs
import artcache.utils.nfs_lock as nfs_lock
import artcache.utils.processing as processing
import artcache.config as config
import artcache.constants as nfs_constants


class LockTest(unittest.TestCase):
    rand_lock_time = None  # type: int
    shared_args = None  # type: List[str]
    PID_FILE_NAME = 'pid'
    ERROR_LOG_FILE_NAME = 'error'

    def setUp(self):
        self.rand_lock_time = random.randint(5, 10)
        self.shared_args = [
            '--lock-time', str(self.rand_lock_time)
        ]
        os.environ['NFSLOCK_SUB_TEST'] = '0'

    def _create_lock_test_tuple(self, non_blocking):
        # type: (object, bool) -> Tuple[bool, List[str]]
        subp_args = copy.deepcopy(self.shared_args)
        if non_blocking:
            subp_args.append('--non-block')
        return non_blocking, subp_args

    def _get_pids(self, temp_dir, count):
        # type: (str, int) -> Dict[int, int]
        pids = {}
        for i in range(0, count):
            pid_file_path = os.path.join(temp_dir,
                                         self.PID_FILE_NAME + str(i))
            with open(pid_file_path, 'r') as pid_file:
                pids[i] = int(pid_file.read())
        return pids

    def _common_run(self, *lock_test_tuples):
        # type: (*Tuple[bool, List[str]]) -> None
        temp_dir = None
        try:
            temp_dir = tempfile.mkdtemp(prefix='test_lock')
            os.environ[nfs_constants.ARTIFACTORY_ROOT_ENV_VAR] = temp_dir
            lock_test_tuple_count = len(lock_test_tuples)
            subps = {}
            for i in range(0, lock_test_tuple_count):
                subp_arg_list = lock_test_tuples[i][1]
                subp_arg_list.extend(['--test-sub-pid', str(i)])
                subp = processing.create_python_subprocess(__file__, None, None, None,
                                                           arguments=subp_arg_list,
                                                           daemon=True)
                subps[i] = subp
                # sleep a quarter of a second to avoid race condition
                time.sleep(.250)
            # double the time if all locks succeed
            wait_time = self.rand_lock_time * lock_test_tuple_count
            processing.wait_for_subprocesses(wait_time, *subps.values())
            killed_count = processing.kill_running_subproccesses(*subps.values())
            # sub processes are spawning daemons and should never last longer than the wait time
            self.assertEqual(killed_count, 0)
            # Wait for daemons to end
            pids = self._get_pids(temp_dir, lock_test_tuple_count)
            processing.wait_for_processes(wait_time, *pids.values())
            killed_count = processing.kill_running_processes(*pids.values())
            # All processes should have finished within the wait time
            self.assertEqual(killed_count, 0)
            lock_history_path = os.path.join(temp_dir,
                                             nfs_constants.ARTIFACTORY_LOCK_HISTORY_FILE)
            lock_history = nfs_lock.get_lock_history(lock_history_path)
            lock_history_index = 0
            for i in range(0, lock_test_tuple_count):
                if i == 0 or not lock_test_tuples[i][0]:
                    # make sure the correct process acquired the lock
                    self.assertEqual(lock_history[lock_history_index][1],
                                     pids[i])
                    self.assertEqual(lock_history[lock_history_index][4],
                                     nfs_lock.LOCK_HISTORY_ACTION_ACQUIRE)
                    lock_history_index += 1
                    # make sure the correct process released the lock
                    self.assertEqual(lock_history[lock_history_index][1],
                                     pids[i])
                    self.assertEqual(lock_history[lock_history_index][4],
                                     nfs_lock.LOCK_HISTORY_ACTION_RELEASE)
                    # check lock time, allow within an additional second for processing load
                    lock_time = lock_history[lock_history_index][0] - lock_history[lock_history_index - 1][0]
                    self.assertTrue(self.rand_lock_time <= lock_time.seconds <= self.rand_lock_time + 1)
                    lock_history_index += 1
        finally:
            if temp_dir is not None:
                shutil.rmtree(temp_dir)

    def test_lock_blocked(self):
        self._common_run(self._create_lock_test_tuple(False),
                         self._create_lock_test_tuple(False))

    def test_lock_unblocked(self):
        self._common_run(self._create_lock_test_tuple(False),
                         self._create_lock_test_tuple(True))


if __name__ == "__main__":

    if 'NFSLOCK_SUB_TEST' not in os.environ:
        unittest.main()
    else:
        parser = argparse.ArgumentParser(prog='LockTest')

        cliargs.add_artifactory_root(parser)

        parser.add_argument('--lock-time',
                            required=True,
                            type=int,
                            help='Time that the lock is held when acquired in seconds.')

        parser.add_argument('--test-sub-pid',
                            required=True,
                            help='Test sub process id, used a file suffix.')

        parser.add_argument('--non-block',
                            action='store_true',
                            default=False,
                            help='Whether to use a non blocking lock.')

        args = parser.parse_args()
        # Set PID file path
        pid_file_path = os.path.join(args.artifactory_root,
                                     LockTest.PID_FILE_NAME + args.test_sub_pid)
        # setup error file
        error_file = open(os.path.join(args.artifactory_root,
                                       LockTest.ERROR_LOG_FILE_NAME + args.test_sub_pid),
                          'w+')
        sys.stderr = error_file
        # Set lock file paths
        lock_file_path = os.path.join(args.artifactory_root,
                                      nfs_constants.ARTIFACTORY_LOCK_FILE)
        lock_history_file_path = os.path.join(args.artifactory_root,
                                              nfs_constants.ARTIFACTORY_LOCK_HISTORY_FILE)
        with nfs_lock.lock_file(lock_file_path, lock_history_file_path, args.non_block) as lfile:
            if lfile is not None:
                time.sleep(args.lock_time)
