import os

TEST_DATA_DIR_NAME = 'TestData'
TEST_DATA_DIR = os.path.join(
    os.path.dirname(
        os.path.dirname(
            os.path.abspath(__file__))),
    TEST_DATA_DIR_NAME)
