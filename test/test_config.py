# Import Standard Lib Modules
import unittest
import sys

# Try to import typing for support
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

sys.dont_write_bytecode = True

# Import Local Lib Modules
import artcache.config as config
import artcache.constants as constants
from ._constants import *


class ConfigTest(unittest.TestCase):

    CONFIG_TEST_DIR_NAME = 'configs'

    def setUp(self):
        pass

    def test_build_info(self):
        json_path = os.path.join(
            TEST_DATA_DIR,
            self.CONFIG_TEST_DIR_NAME,
            constants.BUILD_INFO_CONFIG_FILE_NAME)
        build_info = config.BuildInfo.get_config(json_path)
        build_info2 = config.BuildInfo.from_json(build_info.to_json())
        self.assertEqual(build_info, build_info2)

    def test_project(self):
        json_path = os.path.join(
            TEST_DATA_DIR,
            self.CONFIG_TEST_DIR_NAME,
            constants.PROJECT_CONFIG_DEFAULT_FILE_NAME)
        project = config.ProjectConfig.get_config(json_path)
        project2 = config.ProjectConfig.from_json(project.to_json())
        self.assertEqual(project, project2)


if __name__ == "__main__":
    unittest.main()
