# Import Standard Lib Modules
import unittest
import os
import sys
import tempfile
import subprocess
import platform

# Try to import typing for support
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

sys.dont_write_bytecode = True

# Import Local Lib Modules
import artcache.config as nfs_config
import artcache.constants as nfs_constants
import artcache.artifactory.artifactorycache as artifactorycache
import artcache.artcacher
from artcache.utils.common import delete_dir, find_files_of_type
from artcache.utils.processing import SUBPROCESS_STD_ARGS
from ._constants import TEST_DATA_DIR


class ArtifactoryTest(unittest.TestCase):
    ROOT_DIR = os.path.join(TEST_DATA_DIR, 'artifactory')

    # CPP Test Vars
    CPP_TEST_DIR = os.path.join(TEST_DATA_DIR, 'cpp')
    CPP_LIB_PROJECT = os.path.join(CPP_TEST_DIR, 'clib')
    CPP_PROG_PROJECT = os.path.join(CPP_TEST_DIR, 'cprog')
    CLIB_SOURCE_VAR = 'CLIB_SOURCE'

    # Java Test Vars
    JAVA_TEST_DIR = os.path.join(TEST_DATA_DIR, 'java')
    JAVA_LIB_PROJECT = os.path.join(JAVA_TEST_DIR, 'jlib')
    JAVA_PROG_PROJECT = os.path.join(JAVA_TEST_DIR, 'jprog')

    # Python Test Vars
    PYTHON_TEST_DIR = os.path.join(TEST_DATA_DIR, 'python')
    PYTHON_LIB_PROJECT = os.path.join(PYTHON_TEST_DIR, 'pylib')
    PYTHON_PROG_PROJECT = os.path.join(PYTHON_TEST_DIR, 'pyprog')

    def setUp(self):
        # test create
        try:
            self.artifactory = artifactorycache.ArtifactoryCache.create(self.ROOT_DIR)
            os.environ[nfs_constants.ARTIFACTORY_ROOT_ENV_VAR] = self.ROOT_DIR
        except Exception as ex:
            if os.path.exists(self.ROOT_DIR):
                delete_dir(self.ROOT_DIR)

    def _upload_artifacts(self,
                          project_dir,  # type: str
                          artifacts,  # type: List[nfs_config.ArtifactIDConfig]
                          ):
        # type: (...) -> None
        os.environ['PROJECT_DIR'] = os.path.realpath(project_dir)
        for artifact in artifacts:
            artifact_id = artifact.expandvars()
            args = ['upload',
                    '--artifactory-root', self.ROOT_DIR,
                    '--project-config',
                    os.path.join(project_dir,
                                 nfs_constants.PROJECT_CONFIG_DEFAULT_FILE_NAME)]
            for field, arg_info in artcache.artcacher.ARTIFACT_ID_KEY_OPTION_MAP.items():
                args.append(arg_info[0])
                args.append(artifact_id.__dict__[field])
            artcache.artcacher.main(*args)

    def _run_in_env(self, project_dir, cmds_arr, dep_sets=None, cwd=None):
        # type: (str, List[List[str]], Optional[List[str]], Optional[str]) -> str
        output = ''
        if platform.system().lower() == 'linux':
            commandGen = lambda file_name, cmd: ['/bin/bash', '-c', 'source ' + file_name + '; ' + ' '.join(cmd)]
        elif platform.system().lower() == 'windows':
            commandGen = lambda file_name, cmd: [
                'wsl.exe',
                '/bin/bash',
                '-c',
                'source $(wslpath ' + file_name.replace('\\', '\\\\') + '); ' + ' '.join(cmd)]
        else:
            raise RuntimeError('System not supported')
        file_name = None
        try:
            # Set temp file to not delete, on windows the subprocess will not be able to read it until it is closed
            with tempfile.NamedTemporaryFile(delete=False) as temp:
                file_name = temp.name
                if dep_sets is not None:
                    args = ['create-env',
                            '--env-type', 'bash',
                            '--project-config',
                            os.path.join(project_dir,
                                         nfs_constants.PROJECT_CONFIG_DEFAULT_FILE_NAME),
                            '--outfile', temp.name]
                    args.extend(dep_sets)
                    artcache.artcacher.main(*args)
            for cmd in cmds_arr:
                if platform.system().lower() == 'windows':
                    for i in range(len(cmd)):
                        cmd[i] = cmd[i].replace('\\', '\\\\')
                        if cmd[i].startswith('C:\\'):
                            cmd[i] = '$(wslpath ' + cmd[i] + ')'
                command = commandGen(file_name, cmd)
                output += str(subprocess.check_output(command, cwd=cwd, **SUBPROCESS_STD_ARGS))
        finally:
            if file_name is not None and os.path.exists(file_name):
                os.remove(file_name)
        return output

    def _create_artifacts(self,
                          project_dir,  # type: str
                          build_commands,  # type: List[List[str]]
                          artifacts=None,  # type: Optional[List[nfs_config.ArtifactIDConfig]]
                          dep_sets=None  # type: Optional[List[str]]
                          ):
        # type: (...) -> None
        os.environ['PROJECT_DIR'] = os.path.realpath(project_dir)
        build_dir = None
        try:
            build_dir = tempfile.mkdtemp()
            os.environ['BUILD_DIR'] = os.path.realpath(build_dir)
            output = self._run_in_env(project_dir, build_commands, dep_sets=dep_sets, cwd=build_dir)
            # print build output
            print(output)
            if artifacts is not None:
                self._upload_artifacts(project_dir, artifacts)
        finally:
            if build_dir is not None:
                delete_dir(build_dir)

    def test_cpp(self):
        # test cpp project

        def cpp_build_and_upload(
                project_dir,  # type: str
                artifacts=None,  # type: Optional[List[nfs_config.ArtifactIDConfig]]
                dep_sets=None,  # type: Optional[Union[List[str]]]
                *cmake_options  # type: str
        ):
            # type: (...) -> None
            call = ['cmake']
            if cmake_options:
                call.extend(cmake_options)
            call.append(project_dir)
            self._create_artifacts(project_dir, [call, ['make']], artifacts, dep_sets)

        # lib artifact
        lib_artifact_id = nfs_config.ArtifactIDConfig()
        lib_artifact_id.name = 'clib-${' + self.CLIB_SOURCE_VAR + '}'
        lib_artifact_id.version = '1.0.0'
        lib_artifact_id.origination_source = 'test'
        # include artifact
        include_artifact_id = nfs_config.ArtifactIDConfig()
        include_artifact_id.name = 'clib-dev'
        include_artifact_id.version = '1.0.0'
        include_artifact_id.origination_source = 'test'
        # build and upload lib a and dev
        os.environ[self.CLIB_SOURCE_VAR] = 'a'
        cpp_build_and_upload(self.CPP_LIB_PROJECT, [lib_artifact_id, include_artifact_id])
        # build and upload lib b
        os.environ[self.CLIB_SOURCE_VAR] = 'b'
        cpp_build_and_upload(self.CPP_LIB_PROJECT, [lib_artifact_id])
        # exec artifact
        exec_artifact_id = nfs_config.ArtifactIDConfig()
        exec_artifact_id.name = 'cprog'
        exec_artifact_id.version = '1.0.0'
        exec_artifact_id.origination_source = 'test'
        cpp_build_and_upload(self.CPP_PROG_PROJECT, [exec_artifact_id], ['build-deps'])
        # exec and assert output of b
        prog_output = self._run_in_env(self.CPP_PROG_PROJECT, [['cprog']], ['run-deps'])
        self.assertEqual(prog_output, os.environ[self.CLIB_SOURCE_VAR])
        # exec and assert output of a
        os.environ[self.CLIB_SOURCE_VAR] = 'a'
        prog_output = self._run_in_env(self.CPP_PROG_PROJECT, [['cprog']], ['run-deps'])
        self.assertEqual(prog_output, os.environ[self.CLIB_SOURCE_VAR])

    def test_java(self):
        # test java project

        def java_build_and_upload(
                project_dir,  # type: str
                artifacts=None,  # type: Optional[List[nfs_config.ArtifactIDConfig]]
                main_class=None,  # type: Optional[str]
                dep_sets=None,  # type: Optional[Union[List[str]]]
        ):
            # type: (...) -> None
            src_path = os.path.join(project_dir, 'src')
            java_files = find_files_of_type('.java', src_path)
            java_compile = ['javac', '-d', '.']
            java_compile.extend(java_files)
            jar_create = ['jar', 'cvf', os.path.basename(project_dir) + '.jar']
            if main_class is not None:
                jar_create[1] += 'e'
                jar_create.append(main_class)
            jar_create.extend(
                [java_file.replace(src_path + '/', '').replace('.java', '.class')
                 for java_file in java_files])
            self._create_artifacts(project_dir, [java_compile, jar_create], artifacts, dep_sets)

        # jlib artifact
        lib_artifact_id = nfs_config.ArtifactIDConfig()
        lib_artifact_id.name = 'jlib'
        lib_artifact_id.version = '1.0.0'
        lib_artifact_id.origination_source = 'test'
        java_build_and_upload(
            self.JAVA_LIB_PROJECT,
            [lib_artifact_id],
            'jlib.TestLib')
        lib_out = self._run_in_env(
            self.JAVA_LIB_PROJECT,
            [['java', 'jlib.TestLib']],
            dep_sets=['exec']
        )
        # jprog artifact
        prog_artifact_id = nfs_config.ArtifactIDConfig()
        prog_artifact_id.name = 'jprog'
        prog_artifact_id.version = '1.0.0'
        prog_artifact_id.origination_source = 'test'
        java_build_and_upload(
            self.JAVA_PROG_PROJECT,
            [prog_artifact_id],
            'jprog.TestProg',
            dep_sets=['libs'])
        prog_out = self._run_in_env(
            self.JAVA_PROG_PROJECT,
            [['java', 'jprog.TestProg']],
            dep_sets=['exec']
        )
        self.assertEqual(lib_out, prog_out)

    def test_python(self):
        # test python project
        # jlib artifact
        lib_artifact_id = nfs_config.ArtifactIDConfig()
        lib_artifact_id.name = 'pylib'
        lib_artifact_id.version = '1.0.0'
        lib_artifact_id.origination_source = 'test'
        self._upload_artifacts(self.PYTHON_LIB_PROJECT, [lib_artifact_id])
        lib_out = self._run_in_env(
            self.PYTHON_LIB_PROJECT,
            [[sys.executable, '-m', 'libtest']],
            dep_sets=['exec']
        )
        # jprog artifact
        prog_artifact_id = nfs_config.ArtifactIDConfig()
        prog_artifact_id.name = 'pyprog'
        prog_artifact_id.version = '1.0.0'
        prog_artifact_id.origination_source = 'test'
        self._upload_artifacts(self.PYTHON_PROG_PROJECT, [prog_artifact_id])
        prog_out = self._run_in_env(
            self.PYTHON_PROG_PROJECT,
            [[sys.executable, '-m', 'progtest']],
            dep_sets=['exec']
        )
        self.assertEqual(lib_out, prog_out)

    def test_clean(self):
        # test clean
        pass

    def tearDown(self):
        if os.path.exists(self.ROOT_DIR):
            delete_dir(self.ROOT_DIR)


if __name__ == "__main__":
    unittest.main()
