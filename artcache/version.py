# Import Standard Lib Modules
import os
import imp
import sys

# Try to import typing for support
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

# Import Local Lib Modules
from artcache.constants import VERSION


_VERSION_STRING_SEPARATOR = '.'


def version_to_string(version):
    # type: (Union[str, List[int]]) -> str
    if isinstance(version, str):
        return version
    return _VERSION_STRING_SEPARATOR.join([str(version_item)
                                           for version_item in version])


def version_from_string(version):
    # type: (Union[str, List[int]]) -> List[int]
    if isinstance(version, list):
        return version
    return [int(version_item)
            for version_item in
            version.split(_VERSION_STRING_SEPARATOR)]


class VersionedClass(object):

    def __init__(self):
        super(VersionedClass, self).__init__()
        self.nfsa_version = VERSION

    @staticmethod
    def sort_key(obj):
        # type: (VersionedClass) -> List[int]
        return version_from_string(obj.nfsa_version)
