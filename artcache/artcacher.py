#!/usr/bin/env python2

# Import Standard Lib Modules
import argparse
import os
import sys

# Try to import typing for support
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

sys.dont_write_bytecode = True

# Import Local Lib Modules
import artcache.utils.cliargs as cliargs
import artcache.config as nfsa_config
import artcache.artifactory.artifactorycache as artifactorycache
from artcache.env_processor import ENV_TYPES, DEFAULT_ENV_TYPE

ARTIFACT_ID_OPTION_PREFIX = '--artifact-id-'
ARTIFACT_ID_DEST_PREFIX = 'artifact_id_'

ARTIFACT_ID_KEYS = nfsa_config.ArtifactIDConfig().get_fields()

ARTIFACT_ID_KEY_OPTION_MAP = dict(
    zip(ARTIFACT_ID_KEYS,
        [(ARTIFACT_ID_OPTION_PREFIX + key.replace('_', '-'), ARTIFACT_ID_DEST_PREFIX + key) for key in ARTIFACT_ID_KEYS],
        ))


class Action(object):

    def __init__(self, description):
        # type: (str) -> None
        super(Action, self).__init__()
        self.name = self.get_name()
        self.parser = argparse.ArgumentParser(prog=self.get_name(),
                                              description=description,
                                              add_help=True,
                                              formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    def __call__(self, *args):
        # type: (*str) -> None
        args2 = self.parser.parse_args(args)
        self.run(args2)

    def add_artifact_id_args(self, required, default=None):
        for option_field, option in ARTIFACT_ID_KEY_OPTION_MAP.items():
            self.parser.add_argument(option[0],
                                     dest=option[1],
                                     required=True,
                                     default=None,
                                     help='Artifact ID value: ' + option_field)

    def get_artifact_id_from_args(self, args):
        artifact_id = nfsa_config.ArtifactIDConfig()
        for option_field, option in ARTIFACT_ID_KEY_OPTION_MAP.items():
            artifact_id.__dict__[option_field] = args.__dict__[option[1]]
        return artifact_id

    def run(self, args):
        # type: (Any) -> None
        raise NotImplementedError()

    def get_help(self):
        # type: () -> str
        import io
        help_io = io.BytesIO()
        self.parser.print_help(help_io)
        return str(help_io.getvalue())

    @classmethod
    def get_name(cls):
        raise NotImplementedError()


class ActionInit(Action):

    def __init__(self):
        # type: () -> None
        super(ActionInit, self).__init__('Creates an nfs artifactory')

        cliargs.add_artifactory_root(self.parser)

    def run(self, args):
        artifactorycache.ArtifactoryCache.create(args.artifactory_root)

    @classmethod
    def get_name(cls):
        return 'init'


class ActionUpload(Action):

    def __init__(self):
        # type: () -> None
        super(ActionUpload, self).__init__('Uploads an artifact to nfs artifactory')

        cliargs.add_artifactory_root(self.parser)

        cliargs.add_project_config(self.parser)

        self.add_artifact_id_args(required=True)

    def run(self, args):

        artifact_id = self.get_artifact_id_from_args(args)

        # Load project config
        project_config_path = args.project_config
        project_config = nfsa_config.ProjectConfig.get_config(project_config_path).expandvars()

        # Load artifactory
        artifactory = artifactorycache.ArtifactoryCache(args.artifactory_root)
        # Get artifact info
        if artifact_id.name not in project_config.artifacts:
            raise RuntimeError(
                'Artifact does not exist in project. Config: ' + project_config_path + '; Artifact: ' + artifact_id.name)
        artifact_info = project_config.artifacts[artifact_id.name]
        # TODO: construct build info
        build_info = nfsa_config.BuildInfo()
        # Upload artifact
        artifactory.upload(os.path.dirname(project_config_path), artifact_id, artifact_info, build_info)

    @classmethod
    def get_name(cls):
        return 'upload'


class ActionSearch(Action):

    def __init__(self):
        # type: () -> None
        super(ActionSearch, self).__init__('Finds an artifact in the nfs artifactory')

        cliargs.add_artifactory_root(self.parser)

        self.add_artifact_id_args(required=False, default='*')

    def run(self, args):

        artifact_id = self.get_artifact_id_from_args(args)

        # Load artifactory
        artifactory = artifactorycache.ArtifactoryCache(args.artifactory_root)
        artifacts = artifactory.search(artifact_id)

        artifact_data = [ARTIFACT_ID_KEYS]
        for artifact in artifacts:
            artifact_data.append([artifact.__dict__[id_key] for id_key in ARTIFACT_ID_KEYS])

        max_lens = [max(artifact_data, key=lambda x: len(x[i])) for i in range(len(ARTIFACT_ID_KEYS))]

        for data in artifact_data:
            line = ''
            for i in range(len(data)):
                line += data[i] + (' ' * (max_lens[i] + 2 - len(data[i])))
            print(line)

    @classmethod
    def get_name(cls):
        return 'search'


class ActionCache(Action):

    def __init__(self):
        # type: () -> None
        super(ActionCache, self).__init__('Caches the artifact locally')

    def run(self, args):
        pass

    @classmethod
    def get_name(cls):
        return 'cache'


class ActionCreateEnv(Action):

    def __init__(self):
        # type: () -> None
        super(ActionCreateEnv, self).__init__('Creates an env file with the necessary artifact dependencies for a '
                                              + 'project. Caching missing artifacts locally.')

        cliargs.add_artifactory_root(self.parser)

        cliargs.add_project_config(self.parser)

        self.parser.add_argument('--outfile',
                                 help='Output env file',
                                 required=False,
                                 type=argparse.FileType('w'),
                                 default=sys.stdout)

        self.parser.add_argument('--env_type',
                                 help='Dependency sets to resolve with env file',
                                 choices=ENV_TYPES.keys(),
                                 default=DEFAULT_ENV_TYPE)

        self.parser.add_argument('dep_sets',
                                 help='Dependency sets to resolve with env file',
                                 nargs='+')

    def run(self, args):
        project_config = nfsa_config.ProjectConfig.get_config(args.project_config).expandvars()
        artifactory = artifactorycache.ArtifactoryCache(args.artifactory_root)
        env = artifactory.create_env(project_config.dependency_sets, args.dep_sets)
        for env_key, env_value in env.items():
            args.outfile.write('export ' + env_key + '="' + env_value + '"' + ENV_TYPES[args.env_type][1])

    @classmethod
    def get_name(cls):
        return 'create-env'


ACTIONS = [
    ActionInit,
    ActionUpload,
    ActionSearch,
    ActionCache,
    ActionCreateEnv
]

ACTION_MAP = dict(zip([action.get_name() for action in ACTIONS], ACTIONS))


def main(*args):

    global ACTION_MAP

    action_names_sorted = sorted(ACTION_MAP.keys())

    parser = argparse.ArgumentParser(prog='NFSArtifactory',
                                     description='Preforms Artifactory Actions',
                                     add_help=False,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('action', choices=action_names_sorted, default=False)
    parser.add_argument('-h --help', dest='help', action='store_true', default=False)

    # print help
    if len(args) == 2 and (args[1] == '-h' or args[1] == '--help'):
        cols = 50
        if 'COLUMNS' in os.environ:
            cols = min(cols, os.environ['COLUMNS'])
        parser.print_help()
        action_break = '\n' + '#' * cols + '\n'
        for action_name in action_names_sorted:
            # print empty line to separate
            print(action_break)
            action_help = ACTION_MAP[action_name]().get_help()
            for line in action_help.splitlines():
                print('\t' + line)
        sys.exit(0)

    args, unknown = parser.parse_known_args(args)

    ACTION_MAP[args.action]()(*unknown)


if __name__ == "__main__":
    main(*sys.argv[1:])
