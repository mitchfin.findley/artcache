# Import Standard Lib Modules
import os

# Try to import typing module (standard in 3.5+)
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

# Import Local Lib Modules
import artcache.config as config
import artcache.constants as constants
import artcache.env_processor as env_processor


class ArtifactoryCacheManagerConfig(config.ConfigBase):

    def __init__(self):
        super(ArtifactoryCacheManagerConfig, self).__init__()
        self.repo = None
        self.branch = None
        self.commit_id = None
        self.arch = None
        self.os = None
        self.env = {}

    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {
        }


class ArtifactoryCacheManager(object):

    def __init__(self, config_path):
        # type: (str) -> None
        super(ArtifactoryCacheManager, self).__init__()
        self._config_path = config_path
        self.config = ArtifactoryCacheManagerConfig.get_config(config_path)
        self.env_processor = env_processor.EnvProcessor()

    def get_config_path(self):
        # type: () -> str
        return self._config_path

    def _get_dependency_set_resolution_order(self, dependency_sets, dependency_set_name):
        #
        if dependency_set_name not in dependency_sets:
            raise RuntimeError('Dependency set does not exist: ' + dependency_set_name)
        resolution_order = []

        def add_to_resolution_order(name):
            if name in resolution_order:
                # Remove to eliminate duplicates
                resolution_order.remove(name)
            # Add to end of resolution
            resolution_order.append(name)

        for sub_set in dependency_sets[dependency_set_name].sub_sets:
            sub_set_res_order = self._get_dependency_set_resolution_order(dependency_sets, sub_set)
            for set_name in sub_set_res_order:
                add_to_resolution_order(set_name)
        # Resolve parent last
        add_to_resolution_order(dependency_set_name)
        return resolution_order

    def _resolve_dependency_set(self, dependency_set):
        # type: (config.DependencySetConfig) -> List[Tuple[str, config.ArtifactIDConfig, config.ArtifactInfoConfig]]
        resolved_deps = []
        for name, a_dep in dependency_set.artifactory.items():
            artifact_id_config = config.ArtifactIDConfig()
            artifact_id_config.name = name
            artifact_id_config.version = a_dep.version
            artifact_id_config.origination_source = a_dep.origination_source
            artifact_paths = self.search_for_artifact(artifact_id_config)
            if len(artifact_paths) != 1:
                raise RuntimeError('Could not resolve/find dependency: ' + artifact_id_config.to_json())
            path = artifact_paths[0]
            artifact_info_config = config.ArtifactInfoConfig.get_config(
                os.path.join(path,
                             constants.ARTIFACT_INFO_CONFIG_FILE_NAME))
            resolved_deps.append((path, artifact_id_config, artifact_info_config))
        for name, l_dep in dependency_set.local.items():
            artifact_id_config = config.ArtifactIDConfig()
            artifact_id_config.name = name
            artifact_id_config.origination_source = None
            artifact_info_config = config.ArtifactInfoConfig()
            artifact_info_config.type = l_dep.artifact_type
            artifact_info_config.language = l_dep.language
            resolved_deps.append((l_dep.path, artifact_id_config, artifact_info_config))
        return resolved_deps

    def _create_env(self, dependency_sets, dependency_set_name, env_type, env=None):
        # type: (Dict[str, config.DependencySetConfig], str, str, Optional[Dict[str, str]]) -> Dict[str, str]
        if env is None:
            env = {}
        resolved_deps = self.resolve_dependency_set(dependency_sets, dependency_set_name)
        for resolved_dep in resolved_deps:
            env = self.env_processor.process(resolved_dep[1], resolved_dep[2], resolved_dep[0], env_type, env)
        return env

    def search(self, artifact_id):
        # type: (config.ArtifactIDConfig) -> List[str]
        return self.search_for_artifact(artifact_id)

    def create_env(self, dependency_sets, dependency_set_names, env=None):
        # type: (Dict[str, config.DependencySetConfig], List[str], Optional[Dict[str, str]]) -> Dict[str, str]
        if env is None:
            env = {}
        for dependency_set_name in dependency_set_names:
            # TODO pass sets remove redundant adds?
            self._create_env(dependency_sets, dependency_set_name, env)
        return env

    def resolve_dependency_set(self, dependency_sets, dependency_set_name):
        # type: (Dict[str, config.DependencySetConfig], str) -> List[Tuple[str, config.ArtifactIDConfig, config.ArtifactInfoConfig]]
        resolution_order = self._get_dependency_set_resolution_order(dependency_sets, dependency_set_name)
        resolved_deps = []
        resolved_dep_paths = []
        for set_name in resolution_order:
            for resolved_dep in self._resolve_dependency_set(dependency_sets[set_name]):
                if resolved_dep[0] not in resolved_dep_paths:
                    resolved_deps.append(resolved_dep)
                    resolved_dep_paths.append(resolved_dep[0])
        return resolved_deps

    def search_for_artifact(self, artifact_id):
        # type: (config.ArtifactIDConfig) -> List[str]
        pass

