# Import Standard Lib Modules
import datetime
import os
import subprocess
import sys
import time
import signal
import platform

# Try to import typing for support
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass


class MultiIO(object):

    def __init__(self, *files):
        self._files = files

    def __del__(self):
        for f in self._files:
            if f != sys.__stdout__:
                try:
                    f.close()
                except:
                    # ignore errors
                    # worst case file will close with program term
                    pass

    def write(self, data):
        for f in self._files:
            f.write(data)

    def flush(self):
        for f in self._files:
            f.flush()


if platform.system().lower() == 'linux':
    import errno

    SUBPROCESS_STD_ARGS = {
    }

    SUBPROCESS_DAEMON_ARGS = {
        'start_new_session': True,
        'close_fds': True
    }

    def is_pid_running(pid):
        # type: (int) -> bool
        """Check whether pid exists in the current process table."""
        if pid < 0:
            return False
        try:
            os.kill(pid, 0)
        except OSError as e:
            return e.errno == errno.EPERM
        else:
            return True

elif platform.system().lower() == 'windows':

    SUBPROCESS_STD_ARGS = {
        'creationflags': subprocess.CREATE_NO_WINDOW
    }

    SUBPROCESS_DAEMON_ARGS = {
        'creationflags': subprocess.DETACHED_PROCESS,
        'close_fds': True
    }

    def is_pid_running(pid):
        # type: (int) -> bool
        """Check whether pid exists in the current process table."""
        import ctypes
        kernel32 = ctypes.windll.kernel32
        HANDLE = ctypes.c_void_p
        DWORD = ctypes.c_ulong
        LPDWORD = ctypes.POINTER(DWORD)

        class ExitCodeProcess(ctypes.Structure):
            _fields_ = [('hProcess', HANDLE),
                        ('lpExitCode', LPDWORD)]

        SYNCHRONIZE = 0x100000
        process = kernel32.OpenProcess(SYNCHRONIZE, 0, pid)
        if not process:
            if kernel32.GetLastError() == 5:
                # Access is denied. This means the process exists!
                return True
            return False

        ec = ExitCodeProcess()
        out = kernel32.GetExitCodeProcess(process, ctypes.byref(ec))
        if not out:
            err = kernel32.GetLastError()
            if err == 5:
                # Access is denied. This means the process exists!
                kernel32.CloseHandle(process)
                return True
            kernel32.CloseHandle(process)
            return False
        elif bool(ec.lpExitCode):
            # There is an exit code, it quit
            kernel32.CloseHandle(process)
            return False
        # No exit code, it's running.
        kernel32.CloseHandle(process)
        return True


def create_python_subprocess(py_file_path,      # type: str
                             stdin,             # type: Optional[Union[BinaryIO, int]]
                             stdout,            # type: Optional[Union[BinaryIO, int]]
                             stderr,            # type: Optional[Union[BinaryIO, int]]
                             cwd=None,          # type: Optional[str]
                             arguments=None,    # type: Optional[List[str]]
                             daemon=False       # type: Optional[bool]
                             ):
    # type: (...) -> subprocess.Popen
    command = [sys.executable, py_file_path]

    if arguments is None:
        arguments = sys.argv

    if len(arguments) > 0:
        command.extend(arguments)

    subprocess_args = dict(SUBPROCESS_STD_ARGS)
    if daemon:
        subprocess_args.update(SUBPROCESS_DAEMON_ARGS)
        if platform.system().lower() == 'windows':
            subprocess_args['creationflags'] |= SUBPROCESS_STD_ARGS['creationflags']

    return subprocess.Popen(command,
                            stdin=stdin,
                            stdout=stdout,
                            stderr=stderr,
                            shell=False,
                            cwd=None,
                            env=os.environ,
                            **subprocess_args)


def linux_double_fork(pid_file=None):
    # type: (str) -> None
    """Daemonize class. UNIX double fork mechanism."""

    try:
        pid = os.fork()
        if pid > 0:
            # exit first parent
            sys.exit(0)
    except OSError as err:
        sys.stderr.write('fork #1 failed: {0}\n'.format(err))
        sys.exit(1)

    # decouple from parent environment
    os.setsid()
    os.umask(0)

    # do second fork
    try:
        pid = os.fork()
        if pid > 0:
            # exit from second parent
            sys.exit(0)
    except OSError as err:
        sys.stderr.write('fork #2 failed: {0}\n'.format(err))
        sys.exit(1)

    # redirect standard file descriptors
    sys.stdout.flush()
    sys.stderr.flush()
    si = open(os.devnull, 'r')
    so = open(os.devnull, 'a+')
    se = open(os.devnull, 'a+')

    os.dup2(si.fileno(), sys.stdin.fileno())
    os.dup2(so.fileno(), sys.stdout.fileno())
    os.dup2(se.fileno(), sys.stderr.fileno())

    if pid_file is not None:
        pid = str(os.getpid())
        with open(pid_file, 'w+') as f:
            f.write(pid + '\n')


def are_subproccesses_running(*subprocesses):
    # type: (*subprocess.Popen) -> bool
    for subp in subprocesses:
        if subp.poll() is None:
            return True
    return False


def kill_running_subproccesses(*subprocesses):
    # type: (*subprocess.Popen) -> int
    processes_killed = 0
    for subp in subprocesses:
        if subp.poll() is None:
            subp.kill()
            processes_killed += 1
    return processes_killed


# seconds can be a floating point number
def wait_for_subprocesses(seconds, *subprocesses):
    # type: (Union[int, float], *subprocess.Popen) -> None
    start = datetime.datetime.now()
    while are_subproccesses_running(*subprocesses) or \
            (seconds is not None and
             ((start - datetime.datetime.now()).seconds < seconds)):
        time.sleep(1)


def are_processes_running(*pids):
    # type: (*int) -> bool
    for pid in pids:
        if is_pid_running(pid):
            return True
    return False


def kill_running_processes(*pids):
    # type: (*int) -> int
    processes_killed = 0
    for pid in pids:
        if is_pid_running(pid):
            os.kill(pid, signal.SIGTERM)
            processes_killed += 1
    return processes_killed


def wait_for_processes(seconds, *pids):
    # type: (Union[int, float], *int) -> None
    start = datetime.datetime.now()
    while are_processes_running(*pids) or \
            (seconds is not None and
             ((start - datetime.datetime.now()).seconds < seconds)):
        time.sleep(1)
