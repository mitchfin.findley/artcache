# Import Standard Lib Modules
import os
import io
import datetime
import socket
import sys
import getpass
import platform
from contextlib import contextmanager

# Try to import typing module (standard in 3.5+)
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass


sys.dont_write_bytecode = True

LOCK_HISTORY_TIME_FORMAT = '%Y-%m-%d-%H-%M-%S'
LOCK_HISTORY_VALUE_SEPARATOR = ' '
LOCK_HISTORY_ACTION_ACQUIRE = 'acquire'
LOCK_HISTORY_ACTION_RELEASE = 'release'


def file_interpret(file_val, allow_none=True):
    # type: (Union[int, str, BinaryIO], bool) -> Optional[BinaryIO]
    if file_val is None:
        if allow_none:
            return None
        else:
            raise ValueError()

    if isinstance(file_val, str):
        return open(file_val, 'w')
    elif isinstance(file_val, int):
        return os.fdopen(file_val, 'w')
    elif isinstance(file_val, io.FileIO):
        return file_val
    else:
        raise TypeError()


if platform.system().lower() == 'linux':

    import fcntl

    def acquire_lock(fd, non_blocking=False):
        # type: (Union[BinaryIO, int], bool) -> None
        # exclusive lock
        op = fcntl.LOCK_EX
        if non_blocking:
            # exclusive non blocking lock
            op = op | fcntl.LOCK_NB
        fcntl.flock(fd, op)


    def release_lock(fd):
        # type: (Union[BinaryIO, int]) -> None
        fcntl.flock(fd, fcntl.LOCK_UN)

    @contextmanager
    def lock_file(lock_file_val, lock_history_file_path=None, non_blocking=False):
        # type: (Union[int, str, BinaryIO], str, bool) -> None
        lock_file_o = file_interpret(lock_file_val, False)
        try:
            acquire_lock(lock_file_o.fileno(), non_blocking)
            if lock_history_file_path is not None:
                lock_history_log_acquire(lock_history_file_path)
            yield lock_file_o
            release_lock(lock_file_o.fileno())
            if lock_history_file_path is not None:
                lock_history_log_release(lock_history_file_path)
        except IOError as ioe:
            yield None

elif platform.system().lower() == 'windows':
    import win32con, win32file, pywintypes

    __overlapped = pywintypes.OVERLAPPED()

    def acquire_lock(fd, non_blocking=False):
        # type: (Union[BinaryIO, int], bool) -> None
        # exclusive lock
        hfile = win32file._get_osfhandle(fd)
        op = win32con.LOCKFILE_EXCLUSIVE_LOCK
        if non_blocking:
            # exclusive non blocking lock
            op = op | win32con.LOCKFILE_FAIL_IMMEDIATELY
        win32file.LockFileEx(hfile, op, 0, 0xffff0000, __overlapped)


    def release_lock(fd):
        # type: (Union[BinaryIO, int]) -> None
        hfile = win32file._get_osfhandle(fd)
        win32file.UnlockFileEx(hfile, 0, 0xffff0000, __overlapped)

    @contextmanager
    def lock_file(lock_file_val, lock_history_file_path=None, non_blocking=False):
        # type: (Union[int, str, BinaryIO], str, bool) -> None
        lock_file_o = file_interpret(lock_file_val, False)
        try:
            acquire_lock(lock_file_o.fileno(), non_blocking)
            if lock_history_file_path is not None:
                lock_history_log_acquire(lock_history_file_path)
            yield lock_file_o
            release_lock(lock_file_o.fileno())
            if lock_history_file_path is not None:
                lock_history_log_release(lock_history_file_path)
        except pywintypes.error as ioe:
            yield None


def _create_lock_history_string(action):
    # type: (str) -> str
    return LOCK_HISTORY_VALUE_SEPARATOR.join(
        [
            datetime.datetime.now().strftime(LOCK_HISTORY_TIME_FORMAT),
            str(os.getpid()),
            socket.gethostname(),
            getpass.getuser(),
            action
        ]
    )


def _convert_lock_history_log_line(line):
    # type: (str) -> Optional[Tuple[datetime.datetime, int, str, str, str]]
    history_log = line.split(LOCK_HISTORY_VALUE_SEPARATOR)
    if len(history_log) != 5:
        return None
    return (datetime.datetime.strptime(history_log[0], LOCK_HISTORY_TIME_FORMAT),
            int(history_log[1]),
            history_log[2],
            history_log[3],
            history_log[4])


def lock_history_log_acquire(history_file):
    # type: (str) -> None
    with open(history_file, 'a+') as hfile:
        hfile.write(_create_lock_history_string(LOCK_HISTORY_ACTION_ACQUIRE) + '\n')


def lock_history_log_release(history_file):
    # type: (str) -> None
    with open(history_file, 'a+') as hfile:
        hfile.write(_create_lock_history_string(LOCK_HISTORY_ACTION_RELEASE) + '\n')


def get_lock_history(history_file):
    # type: (str) -> List[Tuple[datetime.datetime, int, str, str, str]]
    with open(history_file, 'r') as hfile:
        lines = [line.strip() for line in hfile.readlines()]
        history = [_convert_lock_history_log_line(line) for line in lines if len(line) > 0]
        history = [hlog for hlog in history if hlog is not None]
        return history
