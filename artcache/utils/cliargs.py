# Import Standard Lib Modules
import argparse
import os

# Try to import typing module (standard in 3.5+)
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

# Import Local Lib Modules
from ..constants import *

# TODO: Create simple adds for arguments

# Default values
DEFAULT_PROJECT_CONFIG = os.path.join(os.path.abspath(os.getcwd()),
                                      PROJECT_CONFIG_DEFAULT_FILE_NAME)
DEFAULT_ARTIFACTORY_CACHE = os.path.join(os.path.abspath(os.path.expanduser("~")),
                                         '.nfsa', 'cache')


def env_arg_default(env_name, argtype=str, default=None):
    # type: (str, Union[Type, Callable[[str], object]], Optional[object]) -> object
    if env_name in os.environ:
        default = argtype(os.environ[env_name])
    return default


def add_env_arg(parser,         # type: argparse.ArgumentParser
                arg_flags,      # type: str
                arg_env_name,   # type: str
                help=None,      # type: Optional[str]
                argtype=str,    # type: Union[Type, Callable[[str], object]]
                default=None,   # type: Optional[object]
                required=True   # type: Optional[bool]
                ):
    # type: (...) -> None
    default_value = env_arg_default(
        arg_env_name,
        argtype=argtype,
        default=default)
    required = default_value is None and required
    parser.add_argument(arg_flags,
                        required=required,
                        type=argtype,
                        default=default_value,
                        help=help)


def add_artifactory_root(parser, default=None, required=True):
    # type: (argparse.ArgumentParser, Optional[str], Optional[bool]) -> None
    add_env_arg(parser,
                '--artifactory-root',
                ARTIFACTORY_ROOT_ENV_VAR,
                help='Root directory of the artifactory',
                default=default,
                required=required)


def add_artifactory_cache_root(parser, default=None, required=True):
    # type: (argparse.ArgumentParser, Optional[str], Optional[bool]) -> None
    if default is None:
        default = DEFAULT_ARTIFACTORY_CACHE
    add_env_arg(parser,
                '--artifactory-cache-root',
                ARTIFACTORY_CACHE_ROOT_ENV_VAR,
                help='Root directory of the artifactory cache',
                default=default,
                required=required)


def add_project_config(parser, default=None):
    # type: (argparse.ArgumentParser, Optional[object]) -> None
    if default is None and os.path.isfile(DEFAULT_PROJECT_CONFIG):
        default = DEFAULT_PROJECT_CONFIG
    add_env_arg(parser,
                '--project-config',
                PROJECTS_CONFIG_ENV_VAR,
                help='Config file containing project',
                default=default)
