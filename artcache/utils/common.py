# Import Standard Lib Modules
import os
import re
import binascii
import platform

# Try to import typing module (standard in 3.5+)
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass


def generate_random_data_hex():
    return binascii.b2a_hex(os.urandom(15))


def delete_dir_contents(directory):
    # type: (str) -> None
    for root, dirs, files in os.walk(directory, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            path = os.path.join(root, name)
            if os.path.islink(path):
                os.remove(path)
            else:
                os.rmdir(path)


def delete_dir(directory):
    # type: (str) -> None
    delete_dir_contents(directory)
    os.rmdir(directory)


def has_parent_path(path, *parent_paths):
    # type: (str, *str) -> bool
    if not parent_paths:
        return False

    for parent in parent_paths:
        if parent.endswith('/'):
            test_parent = str(parent)
        else:
            test_parent = parent + '/'

        if path.startswith(test_parent):
            return True

    return False


def find_files(regex, search_dir=None, depth=0, max_depth=None, ignore_paths=None):
    # type: (str, Optional[str], Optional[int], Optional[int], Optional[List[str]]) -> List[str]
    if search_dir is None:
        search_dir = os.path.abspath(os.getcwd())
    files = []
    for f in os.listdir(search_dir):
        path = os.path.join(search_dir, f)
        if os.path.isfile(path):
            if re.match(regex, f) and (not ignore_paths or has_parent_path(path, *ignore_paths)):
                files.append(path)
        elif os.path.isdir(path) and (max_depth is None or depth < max_depth):
            files.extend(find_files(regex, path, depth + 1, max_depth, ignore_paths))
    return files


def find_files_of_type(ext, search_dir=None, depth=0, max_depth=None, ignore_paths=None):
    # type: (str, Optional[str], Optional[int], Optional[int], Optional[List[str]]) -> List[str]
    regex = r'.*'
    if not ext.startswith('.'):
        regex += '[.]'
    regex += ext
    return find_files(regex, search_dir, depth, max_depth, ignore_paths)
