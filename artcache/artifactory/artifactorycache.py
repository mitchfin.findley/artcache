# Import Standard Lib Modules
import os
import shutil
import glob

# Try to import typing module (standard in 3.5+)
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

# Import Local Lib Modules
import artcache.config as config
import artcache.constants as constants
import artcache.utils.nfs_lock as nfs_lock
import artcache.utils.common as nfs_common
import artcache.env_processor as env_processor
import artcache.version as nfs_version
from .artifactorybase import ArtifactoryBase


class ArtifactoryCache(ArtifactoryBase):

    def __init__(self, root_dir):
        # type: (str) -> None
        super(ArtifactoryCache, self).__init__()
        self.root_dir = root_dir
        config_path = self._get_config_path()
        self.current_version_str = config.ConfigVersionTester.get_config_version(config_path)
        self.config = config.ArtifactoryConfig.get_config(config_path)
        self.env_processor = env_processor.EnvProcessor()
        if self.current_version_str != constants.VERSION:
            self._migrate()

    def _get_config_path(self):
        # type: () -> str
        return os.path.join(
            self.root_dir,
            constants.ARTIFACTORY_CONFIG_FILE_NAME)

    def _get_dependency_set_resolution_order(self, dependency_sets, dependency_set_name):
        #
        if dependency_set_name not in dependency_sets:
            raise RuntimeError('Dependency set does not exist: ' + dependency_set_name)
        resolution_order = []

        def add_to_resolution_order(name):
            if name in resolution_order:
                # Remove to eliminate duplicates
                resolution_order.remove(name)
            # Add to end of resolution
            resolution_order.append(name)

        for sub_set in dependency_sets[dependency_set_name].sub_sets:
            sub_set_res_order = self._get_dependency_set_resolution_order(dependency_sets, sub_set)
            for set_name in sub_set_res_order:
                add_to_resolution_order(set_name)
        # Resolve parent last
        add_to_resolution_order(dependency_set_name)
        return resolution_order

    def _get_path_for_artifact(self,
                               artifact_id_config):
        return os.path.join(self.get_artifacts_dir(),
                            artifact_id_config.origination_source,
                            artifact_id_config.name,
                            artifact_id_config.version)

    def _resolve_dependency_set(self, dependency_set):
        # type: (config.DependencySetConfig) -> List[Tuple[str, config.ArtifactIDConfig, config.ArtifactInfoConfig]]
        resolved_deps = []
        for name, a_dep in dependency_set.artifactory.items():
            artifact_id_config = config.ArtifactIDConfig()
            artifact_id_config.name = name
            artifact_id_config.version = a_dep.version
            artifact_id_config.origination_source = a_dep.origination_source
            artifact_paths = self.search_for_artifact(artifact_id_config)
            if len(artifact_paths) != 1:
                raise RuntimeError('Could not resolve/find dependency: ' + artifact_id_config.to_json())
            path = artifact_paths[0]
            artifact_info_config = config.ArtifactInfoConfig.get_config(
                os.path.join(path,
                             constants.ARTIFACT_INFO_CONFIG_FILE_NAME))
            resolved_deps.append((path, artifact_id_config, artifact_info_config))
        for name, l_dep in dependency_set.local.items():
            artifact_id_config = config.ArtifactIDConfig()
            artifact_id_config.name = name
            artifact_id_config.origination_source = None
            artifact_info_config = config.ArtifactInfoConfig()
            artifact_info_config.type = l_dep.artifact_type
            artifact_info_config.language = l_dep.language
            resolved_deps.append((l_dep.path, artifact_id_config, artifact_info_config))
        return resolved_deps

    def _create_env(self, dependency_sets, dependency_set_name, env=None):
        # type: (Dict[str, config.DependencySetConfig], str, Optional[Dict[str, str]]) -> Dict[str, str]
        if env is None:
            env = {}
        resolved_deps = self.resolve_dependency_set(dependency_sets, dependency_set_name)
        for resolved_dep in resolved_deps:
            env = self.env_processor.process(resolved_dep[1], resolved_dep[2], resolved_dep[0], env)
        return env

    def _migrate(self):
        # type: () -> None
        from artcache.migration import MigrationArtifactFinder
        with nfs_lock.lock_file(self.get_lock_file_path(), self.get_lock_history_file_path()) as lfile:
            artifact_data_paths = MigrationArtifactFinder().get_all_artifact_data_paths(
                self.current_version_str,
                self.get_version_root_dir(self.current_version_str))
            # Path handler for highest module
            for artifact_data_path in artifact_data_paths:
                # Load and migrate artifact id config
                artifact_id_path = os.path.join(artifact_data_path, constants.ARTIFACT_ID_CONFIG_FILE_NAME)
                artifact_id = config.ArtifactIDConfig.get_config(artifact_id_path)
                new_artifact_path = self.get_path_for_artifact(artifact_id)
                # Create migration link
                self.create_relative_artifactory_link(artifact_data_path, new_artifact_path)
            self.current_version_str = constants.VERSION
            # write new config file, and relink
            # TODO: make standard class function for use with create
            version_config_file = os.path.join(self.get_current_version_root_dir(),
                                               constants.ARTIFACTORY_CONFIG_FILE_NAME)
            with open(version_config_file, 'w') as config_file:
                config_file.write(self.config.to_json())
            self.create_relative_artifactory_link(version_config_file,
                                                  self._get_config_path())

    def create_relative_artifactory_link(self, target_path, link_path):
        # type: (str, str) -> None
        self._create_relative_artifactory_link(self.root_dir, target_path, link_path)

    def get_version_root_dir(self, version):
        # type: (Union[str, List[int]]) -> str
        return self._get_version_root_dir(self.root_dir, version)

    def get_current_version_root_dir(self):
        # type: () -> str
        return self.get_version_root_dir(self.current_version_str)

    def get_lock_file_path(self):
        # type: () -> str
        return os.path.join(self.root_dir,
                            constants.ARTIFACTORY_LOCK_FILE)

    def get_lock_history_file_path(self):
        # type: () -> str
        return os.path.join(self.root_dir,
                            constants.ARTIFACTORY_LOCK_HISTORY_FILE)

    def search(self, artifact_id):
        # type: (config.ArtifactIDConfig) -> List[str]
        return self.search_for_artifact(artifact_id)

    def create_env(self, dependency_sets, dependency_set_names, env=None):
        # type: (Dict[str, config.DependencySetConfig], List[str], Optional[Dict[str, str]]) -> Dict[str, str]
        if env is None:
            env = {}
        for dependency_set_name in dependency_set_names:
            # TODO pass sets remove redundant adds?
            self._create_env(dependency_sets, dependency_set_name, env)
        return env

    def clean(self):
        # type: () -> None
        # TODO: use date modified time on data folders
        pass

    @classmethod
    def _create_relative_artifactory_link(cls, base_path, target_path, link_path):
        # type: (str, str, str) -> None
        base_realpath = os.path.realpath(base_path)
        link_realpath = os.path.realpath(link_path)
        target = os.path.realpath(target_path).replace(base_realpath, '')
        if target.startswith(os.sep):
            target = target[1:]
        target = os.path.join(
            os.sep.join(
                ['..' for x in
                 os.path.dirname(link_realpath).replace(base_realpath, '').split(os.sep)
                 if x != '']),
            target
        )
        link_dir = os.path.dirname(link_realpath)
        if not os.path.exists(link_dir):
            os.makedirs(link_dir)
        os.symlink(target_path, link_realpath)

    @classmethod
    def _get_version_root_dir(cls, root_dir, version):
        # type: (str, List[int]) -> str
        return os.path.join(root_dir,
                            nfs_version.version_to_string(version))

    @classmethod
    def create(cls, root_dir, *args, **kwargs):
        # type: (str, *object, **object) -> ArtifactoryCache
        if os.path.exists(root_dir):
            raise RuntimeError('Cannot create artifactory where one already exists: ' + root_dir)
        version_root_dir = cls._get_version_root_dir(root_dir, constants.VERSION)
        os.makedirs(version_root_dir)
        artifactory_config = config.ArtifactoryConfig()
        version_config_file_path = os.path.join(version_root_dir, constants.ARTIFACTORY_CONFIG_FILE_NAME)
        with open(version_config_file_path, 'w') as config_file:
            config_file.write(artifactory_config.to_json())
        cls._create_relative_artifactory_link(root_dir,
                                              version_config_file_path,
                                              os.path.join(root_dir,
                                                           constants.ARTIFACTORY_CONFIG_FILE_NAME))
        return cls(root_dir=root_dir, *args, **kwargs)

    def get_artifact_file_map(self, project_dir, artifact_config):
        # type: (str, config.ArtifactInfoConfig) -> Dict[str, str]
        file_map = {}
        for included_glob, artifact_file in self.get_artifact_config_file_map(artifact_config).items():
            included_glob_path = os.path.expandvars(included_glob)
            artifact_file_path = os.path.expandvars(artifact_file)
            if not os.path.isabs(included_glob_path):
                included_glob_path = os.path.join(project_dir, included_glob_path)
            if os.path.isabs(artifact_file_path):
                raise RuntimeError("Artifact paths must be relative. Invalid Value: " + artifact_file_path)
            included_files = glob.glob(included_glob_path)
            included_files_count = len(included_files)
            if included_files_count == 1 and included_files[0] == included_glob_path:
                file_map[os.path.realpath(included_files[0])] = artifact_file_path
            else:
                for included_file in included_files:
                    file_map[os.path.realpath(included_file)] = os.path.join(artifact_file_path,
                                                                             os.path.basename(included_file))
        return file_map

    def get_artifacts_dir(self):
        # type: () -> str
        return os.path.join(self.get_current_version_root_dir(),
                            constants.ARTIFACTORY_ARTIFACTS_DIR_NAME)

    def get_new_data_dir_path(self):
        # type: () -> str
        artifactory_version_dir = self.get_current_version_root_dir()
        base_dir = os.path.join(artifactory_version_dir,
                                constants.ARTIFACTORY_DATA_DIR_NAME)
        new_data_dir = os.path.join(base_dir, nfs_common.generate_random_data_hex())
        while os.path.exists(new_data_dir):
            new_data_dir = os.path.join(base_dir, nfs_common.generate_random_data_hex())
        return new_data_dir

    def resolve_dependency_set(self, dependency_sets, dependency_set_name):
        # type: (Dict[str, config.DependencySetConfig], str) -> List[Tuple[str, config.ArtifactIDConfig, config.ArtifactInfoConfig]]
        resolution_order = self._get_dependency_set_resolution_order(dependency_sets, dependency_set_name)
        resolved_deps = []
        resolved_dep_paths = []
        for set_name in resolution_order:
            for resolved_dep in self._resolve_dependency_set(dependency_sets[set_name]):
                if resolved_dep[0] not in resolved_dep_paths:
                    resolved_deps.append(resolved_dep)
                    resolved_dep_paths.append(resolved_dep[0])
        return resolved_deps

    def get_artifact_config_file_map(self, artifact_config):
        # type: (config.ArtifactInfoConfig) -> dict
        return artifact_config.file_map

    def get_path_for_artifact(self, artifact_id_config):
        # type: (config.ArtifactIDConfig) -> str
        return self._get_path_for_artifact(artifact_id_config)

    def extract_artifact_id_from_dependency(self, artifact_name, dependency_config):
        # type: (str, config.ArtifactoryDependencyConfig) -> config.ArtifactIDConfig
        artifact_id_config = config.ArtifactIDConfig()
        artifact_id_config.name = artifact_name
        artifact_id_config.version = dependency_config.version
        artifact_id_config.origination_source = dependency_config.origination_source
        return artifact_id_config

    def get_path_for_dependency(self, artifact_name, dependency_config):
        # type: (str, config.ArtifactoryDependencyConfig) -> str
        artifact_id_config = self.extract_artifact_id_from_dependency(artifact_name, dependency_config)
        return self._get_path_for_artifact(artifact_id_config)

    def get_all_artifact_data_paths(self):
        # type: () -> List[str]
        artifacts = []
        artifacts_dir = self.get_artifacts_dir()
        for source_name in os.listdir(artifacts_dir):
            source_path = os.path.join(artifacts_dir, source_name)
            if os.path.isdir(source_path):
                for artifact_name in os.listdir(source_path):
                    artifact_path = os.path.join(source_path, artifact_name)
                    if os.path.isdir(artifact_path):
                        for version in os.listdir(artifact_path):
                            version_path = os.path.join(artifact_path, version)
                            if os.path.islink(version_path):
                                data_dir = os.readlink(version_path)
                                artifacts.append(data_dir)
        return artifacts

    def search_for_artifact(self, artifact_id):
        # type: (config.ArtifactIDConfig) -> List[str]
        for field in artifact_id.get_fields():
            if artifact_id.__dict__[field] is None:
                artifact_id.__dict__[field] = '*'
        artifacts = [artifact
                     for artifact in glob.glob(self._get_path_for_artifact(artifact_id))
                     if os.path.islink(artifact)]
        # TODO: version resolution
        return [os.path.realpath(artifact) for artifact in artifacts]

    def upload(self,
               project_dir,
               artifact_id_config,
               artifact_info_config,
               build_info,
               force=False):
        # type: (str, config.ArtifactIDConfig, config.ArtifactInfoConfig, config.BuildInfo, Optional[bool]) -> bool
        # Get artifact dir
        artifact_dir = self.get_path_for_artifact(artifact_id_config)
        # Check if artifact already exists
        if os.path.exists(artifact_dir):
            if not force:
                return False
            # Delete old artifact content
            nfs_common.delete_dir_contents(os.readlink(artifact_dir))
        # Lock artifactory
        with nfs_lock.lock_file(self.get_lock_file_path(), self.get_lock_history_file_path()) as lfile:
            file_map = self.get_artifact_file_map(project_dir, artifact_info_config)
            # Get data directory
            data_dir = self.get_new_data_dir_path()
            data_artifact_dir = os.path.join(data_dir, constants.ARTIFACTORY_DATA_ARTIFACT_DIR_NAME)
            # Create data and data artifact dir
            os.makedirs(data_artifact_dir)
            # Save config files
            config_files = {
                constants.BUILD_INFO_CONFIG_FILE_NAME: build_info,
                constants.ARTIFACT_ID_CONFIG_FILE_NAME: artifact_id_config,
                constants.ARTIFACT_INFO_CONFIG_FILE_NAME: artifact_info_config
            }
            for file_name, config_obj in config_files.items():
                # build info can be none, skip
                if config_obj is None:
                    continue
                with open(os.path.join(data_dir, file_name), 'w') as config_file:
                    config_file.write(config_obj.to_json())
            # Copy files
            for included_file, artifact_path in file_map.items():
                dest_path = os.path.join(data_artifact_dir, artifact_path)
                src_path = included_file
                if os.path.exists(src_path):
                    while os.path.islink(src_path):
                        src_path = os.readlink(src_path)
                    # Create destination directory if it does not exist
                    dest_dir_path = os.path.dirname(dest_path)
                    if not os.path.exists(dest_dir_path):
                        os.makedirs(dest_dir_path)
                    if os.path.isfile(src_path):
                        shutil.copy2(src_path, dest_path)
                    elif os.path.isdir(src_path):
                        shutil.copytree(src_path, dest_path, symlinks=False, ignore=None)
                    else:
                        raise RuntimeError("Error Copying artifact file: " + included_file)
            # Create artifact link
            self.create_relative_artifactory_link(data_dir, artifact_dir)
