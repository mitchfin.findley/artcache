# Import Standard Lib Modules
import os
import shutil
import glob

# Try to import typing module (standard in 3.5+)
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

# Import Local Lib Modules
import artcache.config as config


class ArtifactID(config.ConfigBase):

    def __init__(self):
        super(ArtifactID, self).__init__()
        self.name = None
        self.version = None


    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {
        }


class ArtifactoryBase(object):

    def __init__(self):
        # type: () -> None
        super(ArtifactoryBase, self).__init__()
        pass

    def search(self, artifact_id):
        # type: (config.ArtifactIDConfig) -> List[str]
        pass

    def cache(self, artifact_id, directory):
        pass
