# Import Standard Lib Modules
import json
import datetime
import os
import io
import copy
import sys
from abc import ABCMeta

# Try to import typing for support
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

# Import Local Lib Modules
from artcache.version import VersionedClass


class DictConvertible(object):
    DATE_FORMAT = '%Y-%m-%d-%H-%M-%S'

    def __init__(self):
        super(DictConvertible, self).__init__()
        self._from_file = None
        self._load_dir = None
        self._is_root = True

    def get_fields(self):
        ignored_fields = self.get_ignored_on_load_fields()
        return [field_key for field_key, field_value in self.__dict__.items()
                if not field_key.startswith('_')
                and not callable(field_value)
                and field_key not in ignored_fields]

    def expandvars(self):
        # type: () -> Any
        dict_obj = copy.deepcopy(self)
        for field in self.get_fields():
            dict_obj.__dict__[field] = self._expandvars(dict_obj.__dict__[field])
        return dict_obj

    def to_standard_dict(self, allow_none=False):
        # type: (bool) -> dict
        o_dict = {}
        for key, value in self.__dict__.items():
            if not key.startswith('_'):
                if value is None:
                    if allow_none:
                        o_dict[key] = value
                    else:
                        continue
                elif not callable(value):
                    o_dict[key] = self._to_dict_object(value, allow_none)
        return o_dict

    def to_json(self):
        # type: () -> str
        return json.dumps(self.to_standard_dict(), indent=4)

    def post_load_processing(self):
        # type: () -> None
        raise NotImplementedError()

    def __ne__(self, obj):
        return not self == obj

    def __eq__(self, obj):
        if not isinstance(obj, self.__class__):
            return False
        for field in self.get_fields():
            if self.__dict__[field] != obj.__dict__[field]:
                return False
        return True

    @classmethod
    def get_complex_attrs(cls):
        # type: () -> Dict[str, Union[Callable[[str], object], type, Tuple[Union[Type[list], Type[dict]]], type]]
        raise NotImplementedError()

    @classmethod
    def get_ignored_on_load_fields(cls):
        # type: () -> List[str]
        return []

    @classmethod
    def from_json(cls, json_o,
                  is_root=True,
                  from_file=None,
                  load_dir=None,
                  exact=False):
        # type: ([file, str], bool, str, str, bool) -> DictConvertible
        if isinstance(json_o, str):
            return cls.from_standard_dict(json.loads(json_o),
                                          is_root=is_root,
                                          from_file=from_file,
                                          load_dir=load_dir,
                                          exact=exact)
        elif isinstance(json_o, file) or isinstance(json_o, io.IOBase):
            return cls.from_standard_dict(json.load(json_o),
                                          is_root=is_root,
                                          from_file=from_file,
                                          load_dir=load_dir,
                                          exact=exact)
        else:
            raise TypeError('Can not read json from: ' + json_o.__class__.__name__)

    @classmethod
    def from_standard_dict(cls, standard_dict,
                           is_root=True,
                           from_file=None,
                           load_dir=None,
                           exact=False):
        # type: (dict, Optional[bool], Optional[str], Optional[str], Optional[bool]) -> DictConvertible
        cls_obj = cls()
        cls_obj._is_root = is_root
        cls_obj._from_file = from_file
        if load_dir is not None:
            cls_obj._load_dir = load_dir
        elif from_file is not None:
            cls_obj._load_dir = os.path.dirname(from_file)
        complex_attrs = cls.get_complex_attrs()
        for key in cls_obj.__dict__.keys():
            if not key.startswith('_'):
                if key in standard_dict:
                    cls_obj.__dict__[key] = cls._from_dict_object(standard_dict[key],
                                                                  complex_attrs.get(key),
                                                                  cls_obj._load_dir,
                                                                  exact)
                elif exact:
                    raise ValueError("Missing Key: " + cls.__name__ + "." + key)
        cls_obj.post_load_processing()
        return cls_obj

    @staticmethod
    def _expandvars(value):
        if value is not None:
            if isinstance(value, str):
                value = os.path.expandvars(value)
            if isinstance(value, unicode):
                value = os.path.expandvars(str(value))
            elif isinstance(value, dict):
                for key in value.keys():
                    expanded_key = DictConvertible._expandvars(key)
                    value[expanded_key] = DictConvertible._expandvars(value[key])
                    if expanded_key != key:
                        del value[key]
            elif isinstance(value, list):
                for i in range(len(value)):
                    value[i] = DictConvertible._expandvars(value[i])
            elif isinstance(value, DictConvertible):
                value = value.expandvars()
        return value

    @staticmethod
    def _to_dict_object(value, allow_none=False):
        # type: (Any, Optional[bool]) -> Union[str, List, Dict]
        dict_value = value
        if isinstance(value, DictConvertible):
            dict_value = value.to_standard_dict(allow_none)
        elif isinstance(value, datetime.datetime):
            dict_value = value.strftime(DictConvertible.DATE_FORMAT)
        elif isinstance(value, list):
            dict_value = [DictConvertible._to_dict_object(item)
                          for item in value]
        elif isinstance(value, dict):
            dict_value = {key: DictConvertible._to_dict_object(val)
                          for key, val in value.items()}
        return dict_value

    @staticmethod
    def _from_dict_object(value, value_type=None, load_dir=None, exact=False):
        # type: (Any, Optional[Tuple], Optional[str], Optional[bool]) -> Any
        obj_value = value
        if isinstance(obj_value, unicode):
            obj_value = str(obj_value)
        if value_type is not None:
            if isinstance(value_type, type):
                if issubclass(value_type, DictConvertible):
                    obj_value = value_type.from_standard_dict(value,
                                                              is_root=False,
                                                              load_dir=load_dir,
                                                              exact=exact)
                elif value_type == datetime.datetime:
                    obj_value = datetime.datetime.strptime(value, DictConvertible.DATE_FORMAT)
                elif value_type == str:
                    obj_value = str(obj_value)
                else:
                    raise TypeError('Can not convert type from dict object: ' + value_type.__name__)
            elif isinstance(value_type, tuple):
                if len(value_type) == 2:
                    sub_value_type = value_type[0]
                    if sub_value_type == list:
                        if isinstance(value, list):
                            for item in value:
                                item = DictConvertible._from_dict_object(item,
                                                                         value_type[1],
                                                                         load_dir,
                                                                         exact)
                        else:
                            raise TypeError()
                    elif sub_value_type == dict:
                        if isinstance(value, dict):
                            for key, val in value.items():
                                value[key] = DictConvertible._from_dict_object(val,
                                                                               value_type[1],
                                                                               load_dir,
                                                                               exact)
                        else:
                            raise TypeError()
                else:
                    raise ValueError("Invalid complex attr tuple")
            elif callable(value_type):
                return value_type(value)
            else:
                raise ValueError()
        return obj_value


class ConfigBase(VersionedClass, DictConvertible):
    __metaclass__ = ABCMeta

    def __init__(self):
        super(ConfigBase, self).__init__()

    def save_config(self, config_file_path):
        # type: (str) -> NoReturn
        # TODO: replace writes with save function
        with open(config_file_path, 'w') as config_file:
            config_file.write(self.to_json())

    @classmethod
    def get_ignored_on_load_fields(cls):
        return []

    @classmethod
    def get_config(cls, config_file_path):
        # type: (str) -> ConfigBase
        return ConfigVersionTester.resolve_config(
            config_file_path,
            cls
        )


class ConfigVersionTester(ConfigBase):

    def __init__(self):
        super(ConfigVersionTester, self).__init__()

    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {}

    @staticmethod
    def get_config_version(config_file_path):
        # type: (str) -> str
        with io.open(config_file_path, 'r') as json_o:
            config_tester = ConfigVersionTester.from_json(json_o)
        return config_tester.nfsa_version

    @staticmethod
    def resolve_config(config_file_path, config_cls):
        # type: (str, Type[ConfigBase]) -> ConfigBase
        from .migration import ConfigMigrator
        with io.open(config_file_path, 'r') as json_o:
            json_dict = json.load(json_o)
            config_tester = ConfigVersionTester.from_standard_dict(json_dict)
            config = config_cls.from_standard_dict(ConfigMigrator().migrate(json_dict, config_tester.nfsa_version, config_cls))
        return config


class BuildInfo(ConfigBase):

    def __init__(self):
        super(BuildInfo, self).__init__()
        self.repo = None
        self.branch = None
        self.commit_id = None
        self.date = datetime.datetime.now()
        self.arch = None
        self.os = None
        self.env = {}

    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {
            "date": datetime.datetime,
            "env": (dict, str)
        }


class BaseDependencyConfig(ConfigBase):

    def __init__(self):
        super(BaseDependencyConfig, self).__init__()

    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {
        }


class ArtifactoryDependencyConfig(BaseDependencyConfig):

    def __init__(self):
        super(ArtifactoryDependencyConfig, self).__init__()
        self.version = None
        self.origination_source = None


class LocalDependencyConfig(BaseDependencyConfig):

    def __init__(self):
        super(LocalDependencyConfig, self).__init__()
        self.path = None
        self.artifact_type = None
        self.language = None


class DependencySetConfig(ConfigBase):

    def __init__(self):
        super(DependencySetConfig, self).__init__()
        self.artifactory = {}
        self.local = {}
        self.sub_sets = []

    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {
            "artifactory": (dict, ArtifactoryDependencyConfig),
            "local": (dict, LocalDependencyConfig),
            "sub_sets": (list, str)
        }


class ArtifactInfoConfig(ConfigBase):

    def __init__(self):
        super(ArtifactInfoConfig, self).__init__()
        self.type = None
        self.language = None
        self.file_map = {}
        self.exclude = []
        self.bin_dir = None

    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {
            "file_map": (dict, str),
            "exclude": (list, str)
        }


class ArtifactIDConfig(ConfigBase):

    def __init__(self):
        super(ArtifactIDConfig, self).__init__()
        self.name = None
        self.version = None
        self.origination_source = None

    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {
        }


class ProjectConfig(ConfigBase):

    def __init__(self):
        super(ProjectConfig, self).__init__()
        self.artifacts = {}
        self.dependency_sets = {}

    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {
            "artifacts": (dict, ArtifactInfoConfig),
            "dependency_sets": (dict, DependencySetConfig),
        }


class ArtifactoryConfig(ConfigBase):

    def __init__(self):
        ConfigBase.__init__(self)

    def post_load_processing(self):
        pass

    @classmethod
    def get_complex_attrs(cls):
        return {}
