# Import Standard Lib Modules
import os
import copy
from abc import ABCMeta

# Try to import typing for support
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass

# Import Local Lib Modules
from .constants import VERSION


class ConfigMigrationBase(object):

    def __init__(self):
        # type: () -> None
        super(ConfigMigrationBase, self).__init__()

    def migrate(self, json_dict, version_str, config_class):
        # type: (Dict, str, Type[_ConfigMigration]) -> Dict
        raise NotImplementedError()


class _ConfigMigration(ConfigMigrationBase):

    __metaclass__ = ABCMeta

    def __init__(self):
        # type: () -> None
        super(_ConfigMigration, self).__init__()

    def from_version(self):
        # type: () -> str
        raise NotImplementedError()

    def to_version(self):
        # type: () -> str
        raise NotImplementedError()


class ConfigMigrator(ConfigMigrationBase):

    _CONFIG_MIGRATIONS = None

    def __init__(self):
        # type: () -> None
        super(ConfigMigrator, self).__init__()
        if self._CONFIG_MIGRATIONS is None:
            from .migrations import get_config_migrations
            self._CONFIG_MIGRATIONS = get_config_migrations()

    def _get_migration_path(self, version_str, migration_path=None):
        # type: (str, Optional[List[_ConfigMigration]]) -> List[_ConfigMigration] or None
        if migration_path is None:
            migration_path = []
        if version_str == VERSION:
            return migration_path
        if version_str not in self._CONFIG_MIGRATIONS:
            return None
        for migration in self._CONFIG_MIGRATIONS[version_str]:
            # Do not want to edit the original
            copy_migration_path = list(migration_path)
            copy_migration_path.append(migration)
            copy_migration_path = self._get_migration_path(migration.to_version(), copy_migration_path)
            if copy_migration_path is not None:
                return copy_migration_path
        return None

    def migrate(self, json_dict, version_str, config_class):
        migration_path = self._get_migration_path(version_str)
        if migration_path is None:
            raise RuntimeError('Unable to migrate ' + config_class.__name__ +
                               ' from ' + version_str + ' to ' + VERSION)
        migrated_dict = json_dict
        for migration in migration_path:
            migrated_dict = migration.migrate(migrated_dict, version_str, config_class)
            version_str = migration.to_version()
        migrated_dict['nfsa_version'] = VERSION
        return migrated_dict


class MigrationArtifactFinderBase(object):

    def __init__(self):
        # type: () -> None
        super(MigrationArtifactFinderBase, self).__init__()

    def get_all_artifact_data_paths(self, version_str, root_dir_path):
        raise NotImplementedError()


class _MigrationArtifactFinder(MigrationArtifactFinderBase):

    __metaclass__ = ABCMeta

    def __init__(self):
        # type: () -> None
        super(_MigrationArtifactFinder, self).__init__()

    def supported_versions(self):
        # type: () -> List[str]
        raise NotImplementedError()


class MigrationArtifactFinder(MigrationArtifactFinderBase):

    _ARTIFACT_FINDER_MIGRATIONS = None

    def __init__(self):
        # type: () -> None
        super(MigrationArtifactFinder, self).__init__()
        if self._ARTIFACT_FINDER_MIGRATIONS is None:
            from .migrations import get_artifact_finder_migrations
            self._ARTIFACT_FINDER_MIGRATIONS = get_artifact_finder_migrations()

    def get_all_artifact_data_paths(self, version_str, root_dir_path):
        if version_str not in self._ARTIFACT_FINDER_MIGRATIONS:
            raise RuntimeError('Could not find migration artifact finder for: ' + version_str)
        return self._ARTIFACT_FINDER_MIGRATIONS[version_str][0].get_all_artifact_data_paths(version_str, root_dir_path)
