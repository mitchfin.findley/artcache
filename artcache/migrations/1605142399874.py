import os
from ..migration import _ConfigMigration, _MigrationArtifactFinder


class ConfigMigration_1605142399874(_ConfigMigration):

    def from_version(self):
        return '0.0.1'

    def to_version(self):
        return '0.0.2'

    def migrate(self, json_dict, version_str, config_class):
        # both versions have the same configs
        return json_dict


class MigrationArtifactFinder_1605142399874(_MigrationArtifactFinder):

    def supported_versions(self):
        return ['0.0.1', '0.0.2']

    def get_all_artifact_data_paths(self, version_str, root_dir_path):
        # ${artifactory_root}/artifacts/${source}/${artifact_name}/${version}
        artifacts = []
        artifacts_dir = os.path.join(root_dir_path, version_str)
        for source_name in os.listdir(artifacts_dir):
            source_path = os.path.join(artifacts_dir, source_name)
            if os.path.isdir(source_path):
                for artifact_name in os.listdir(source_path):
                    artifact_path = os.path.join(source_path, artifact_name)
                    if os.path.isdir(artifact_path):
                        for version in os.listdir(artifact_path):
                            version_path = os.path.join(artifact_path, version)
                            if os.path.islink(version_path):
                                data_dir = os.readlink(version_path)
                                artifacts.append(data_dir)
        return artifacts
