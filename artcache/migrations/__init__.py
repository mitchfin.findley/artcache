# Import Standard Lib Modules
import os
import inspect
import importlib
import sys

# Try to import typing for support
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass


def _get_migrations(migration_class):
    # type: (Type) -> List[Any]
    migration_objs = []
    _init_module_path = os.path.realpath(__file__)
    _module_dir_path = os.path.dirname(_init_module_path)
    for module_file_name in os.listdir(_module_dir_path):
        module_name = os.path.splitext(module_file_name)[0]
        if module_name != '__init__':
            # migration_module = __import__('.' + module_name)
            migration_module = importlib.import_module('.' + module_name, __name__)
            for name, obj in inspect.getmembers(migration_module):
                if not name.startswith('_') \
                        and inspect.isclass(obj) \
                        and (issubclass(obj, migration_class)):
                    migration_objs.append(obj())
    return migration_objs


def get_config_migrations():
    # type: () -> Dict[str, List[_ConfigMigration]]
    from ..migration import _ConfigMigration
    config_migrations = {}
    for config_migration in _get_migrations(_ConfigMigration):
        from_version = config_migration.from_version()
        if from_version not in config_migrations:
            config_migrations[from_version] = []
        config_migrations[from_version].append(config_migration)
    return config_migrations


def get_artifact_finder_migrations():
    # type: () -> Dict[str, List[_MigrationArtifactFinder]]
    from ..migration import _MigrationArtifactFinder
    migrations = {}
    for migration in _get_migrations(_MigrationArtifactFinder):
        for supported_version in migration.supported_versions():
            if supported_version not in migrations:
                migrations[supported_version] = []
            migrations[supported_version].append(migration)
    return migrations

