# Import Standard Lib Modules
import os
import inspect
import sys
import platform
from abc import ABCMeta

# Try to import typing module (standard in 3.5+)
try:
    from typing import *
except ImportError as ex:
    # ignore
    pass


# Import Local Lib Modules
import artcache.config as nfs_config
from artcache.constants import ARTIFACTORY_DATA_ARTIFACT_DIR_NAME

ENV_TYPES = {
    # env type: (list value separator, line separator)
    'bash': (':', '\n'),
    'bat': (';', '\r\n')
}

PLATFORM_DEFAULT_ENVIRONMENT_TYPES = {
    'linux': 'bash',
    'windows': 'bat'
}

DEFAULT_ENV_TYPE = PLATFORM_DEFAULT_ENVIRONMENT_TYPES[platform.system().lower()]


class _EnvProcessor(object):

    def process(self,
                artifact_id,    # type: nfs_config.ArtifactIDConfig
                artifact_info,  # type: nfs_config.ArtifactInfoConfig
                path,           # type: str
                env_type,       # type: str
                env=None        # type: Optional[Dict[str, str]]
                ):
        # type: (...) -> Dict[str, str]
        raise NotImplementedError()


class _SubEnvProcessor(_EnvProcessor):

    __metaclass__ = ABCMeta

    def is_supported(self, artifact_id, artifact_info):
        # type: (Any, Any) -> True
        raise NotImplementedError()


class EnvProcessor(_EnvProcessor):

    _SUB_ENV_PROCESSORS = None  # type: Union[None, List[_SubEnvProcessor]]

    def process(self, artifact_id, artifact_info, path, env_type, env=None):
        if self._SUB_ENV_PROCESSORS is None:
            self._SUB_ENV_PROCESSORS = []
            for name, obj in inspect.getmembers(sys.modules[__name__]):
                if not name.startswith('_') \
                        and inspect.isclass(obj) \
                        and obj != self.__class__ \
                        and (issubclass(obj, _SubEnvProcessor) or obj == _SubEnvProcessor):
                    self._SUB_ENV_PROCESSORS.append(obj())
        for sub_env_processor in self._SUB_ENV_PROCESSORS:
            if sub_env_processor.is_supported(artifact_id, artifact_info):
                env = sub_env_processor.process(
                    artifact_id,
                    artifact_info,
                    os.path.join(path, ARTIFACTORY_DATA_ARTIFACT_DIR_NAME),
                    env_type,
                    env=env)
                break
        return env


class _LangEnvProcessor(_SubEnvProcessor):

    def process(self, artifact_id, artifact_info, path, env_type, env=None):
        if env is None:
            env = {}
        env[artifact_id.name.replace('-', '_') + '_PATH'] = path
        return env

    def prepend_env_var(self, env, env_var, value, env_type, default=''):
        # type: (Dict[str, str], str, str, Optional[str], Optional[str]) -> Dict[str, str]
        sep = ENV_TYPES[env_type][0]
        if env_var in env:
            env[env_var] = value + sep + env[env_var]
        else:
            env[env_var] = value + sep + os.getenv(env_var, default)
        return env

    def is_supported(self, artifact_id, artifact_info):
        # type: (nfs_config.ArtifactIDConfig, nfs_config.ArtifactInfoConfig) -> bool
        return artifact_info.language in self.langs()

    def langs(self):
        # type: () -> List[str]
        raise NotImplementedError()


class DefaultEnvProcessor(_LangEnvProcessor):

    def langs(self):
        # type: () -> List[str]
        return [None, '', 'data']


class CppEnvProcessor(_LangEnvProcessor):

    def process(self, artifact_id, artifact_info, path, env_type, env=None):
        env = super(CppEnvProcessor, self).process(artifact_id, artifact_info, path, env_type, env=env)
        artifact_type_lower = artifact_info.type.lower()
        if artifact_type_lower == 'include':
            self.prepend_env_var(env, 'CPATH', path, env_type)
        elif artifact_type_lower == 'lib':
            # compile time
            self.prepend_env_var(env, 'LIBRARY_PATH', path, env_type)
            # run time
            self.prepend_env_var(env, 'LD_LIBRARY_PATH', path, env_type)
        elif artifact_type_lower == 'exec':
            self.prepend_env_var(env, 'PATH', path, env_type)
        return env

    def langs(cls):
        return ['c', 'cpp']


class JavaEnvProcessor(_LangEnvProcessor):

    def process(self, artifact_id, artifact_info, path, env_type, env=None):
        env = super(JavaEnvProcessor, self).process(artifact_id, artifact_info, path, env_type, env=env)
        artifact_type_lower = artifact_info.type.lower()
        if artifact_type_lower == 'jar':
            self.prepend_env_var(env, 'CLASSPATH', path + os.sep + '*', env_type)
        return env

    def langs(cls):
        return ['java']


class PythonEnvProcessor(_LangEnvProcessor):

    def process(self, artifact_id, artifact_info, path, env_type, env=None):
        env = super(PythonEnvProcessor, self).process(artifact_id, artifact_info, path, env_type, env=env)
        artifact_type_lower = artifact_info.type.lower()
        if artifact_type_lower == 'source':
            self.prepend_env_var(env, 'PYTHONPATH', path, env_type)
        return env

    def langs(cls):
        return ['python']
